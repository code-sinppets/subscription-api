package com.ezypay.api.subscription.handler;

import com.ezypay.api.subscription.model.SubscriptionType;
import com.ezypay.api.subscription.model.SubscriptionRequest;
import com.ezypay.api.subscription.model.SubscriptionResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SubscriptionHandlerTest {

  private static final String BASE_URL = "/api/v1/subscriptions";

  @Autowired private WebTestClient webClient;

  @Test
  void givenValidWeeklySubscriptionRequest_whenCreate_thenCreateSubscription() {
    var createSubscription =
        SubscriptionRequest.builder()
            .amount(10)
            .subscriptionType(SubscriptionType.WEEKLY)
            .dayOfWeek(DayOfWeek.TUESDAY)
            .startDate(LocalDate.now())
            .endDate(LocalDate.now().plusMonths(1))
            .build();

    var response =
        SubscriptionResponse.builder()
            .amount(10)
            .subscriptionType(SubscriptionType.WEEKLY)
            .invoiceDates(List.of("02/03/2021", "09/03/2021", "16/03/2021", "23/03/2021"))
            .build();

    webClient
        .post()
        .uri(BASE_URL)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .body(Mono.just(createSubscription), SubscriptionRequest.class)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(SubscriptionResponse.class)
        .isEqualTo(response);
  }

  @Test
  void givenValidMonthlySubscriptionRequest_whenCreate_thenCreateSubscription() {
    var createSubscription =
        SubscriptionRequest.builder()
            .amount(10)
            .subscriptionType(SubscriptionType.MONTHLY)
            .month((short) 20)
            .startDate(LocalDate.now())
            .build();

    var actualResponse =
        webClient
            .post()
            .uri(BASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(createSubscription), SubscriptionRequest.class)
            .exchange()
            .expectStatus()
            .isCreated()
            .expectBody(SubscriptionResponse.class)
            .returnResult()
            .getResponseBody();

    assertNotNull(actualResponse);
    ;
    assertEquals(actualResponse.getAmount(), createSubscription.getAmount());
    assertEquals(actualResponse.getSubscriptionType(), createSubscription.getSubscriptionType());
    assertEquals(3, actualResponse.getInvoiceDates().size());
  }

  @Test
  void givenValidDailySubscriptionRequest_whenCreate_thenCreateSubscription() {
    var createSubscription =
        SubscriptionRequest.builder()
            .amount(10)
            .subscriptionType(SubscriptionType.DAILY)
            .startDate(LocalDate.now())
            .endDate(LocalDate.now().plusDays(7))
            .build();

    var actualResponse =
        webClient
            .post()
            .uri(BASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(createSubscription), SubscriptionRequest.class)
            .exchange()
            .expectStatus()
            .isCreated()
            .expectBody(SubscriptionResponse.class)
            .returnResult()
            .getResponseBody();

    assertNotNull(actualResponse);
    ;
    assertEquals(actualResponse.getAmount(), createSubscription.getAmount());
    assertEquals(actualResponse.getSubscriptionType(), createSubscription.getSubscriptionType());
    assertEquals(7, actualResponse.getInvoiceDates().size());
  }
}
