package com.ezypay.api.subscription.utils;

import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DateRangeUtilsTest {

    @Test
    void givenWeeklySubscriptionWithDateRange_whenGetDates_shouldReturnDates() {
        var today = LocalDate.of(2021, Month.FEBRUARY, 25);
        List<String>  dates = DateRangeUtils.getDatesForWeeklySubscription(DayOfWeek.FRIDAY, today, today.plusMonths(1));
        assertNotNull(dates);
        assertEquals(4, dates.size());
    }

    @Test
    void givenMonthlySubscriptionWithDateRange_whenGetDates_shouldReturnDates() {
        var today = LocalDate.of(2021, Month.FEBRUARY, 25);
        List<String>  dates = DateRangeUtils.getDatesForMonthlySubscription((short) 24, today, today.plusMonths(1));
        assertNotNull(dates);
        assertEquals(1, dates.size());
    }
}