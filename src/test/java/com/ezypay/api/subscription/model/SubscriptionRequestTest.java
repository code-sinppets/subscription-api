package com.ezypay.api.subscription.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class SubscriptionRequestTest {

  protected static Validator validator;

  @BeforeAll
  public static void setUpValidator() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  void givenEmptyRequest_whenValidated_shouldFailValidation() {
    var results = validator.validate(SubscriptionRequest.builder().build());
    assertNotNull(results);
    assertEquals(2, results.size());
    assertTrue(getErrorMessages(results).containsAll(List.of("{amount.invalid}", "{subscriptionType.required}" )));
  }

  @Test
  void givenValidObject_whenValidated_shouldPassValidation() {
    var request =
        SubscriptionRequest.builder()
            .amount(10)
            .startDate(LocalDate.now())
            .endDate(LocalDate.now().plusMonths(3))
            .subscriptionType(SubscriptionType.DAILY)
            .build();

    var results = validator.validate(request);
    assertNotNull(results);
    assertEquals(0, results.size());
  }

  @Test
  void givenValidObjectWithNoEndDate_whenValidated_shouldFailValidation() {
    var request =
            SubscriptionRequest.builder()
                    .amount(10)
                    .startDate(LocalDate.now())
                    .subscriptionType(SubscriptionType.DAILY)
                    .build();

    var results = validator.validate(request);
    assertNotNull(results);
    assertEquals(0, results.size());
  }

  @Test
  void givenValidObjectWithNoEndDateRangeMoreThan3Months_whenValidated_shouldFailValidation() {
    var request =
        SubscriptionRequest.builder()
            .amount(10)
            .startDate(LocalDate.now())
            .endDate(LocalDate.now().plusMonths(6))
            .subscriptionType(SubscriptionType.DAILY)
            .build();

    var results = validator.validate(request);
    assertNotNull(results);
    assertEquals(1, results.size());
    assertTrue(getErrorMessages(results).contains("{date.range.invalid}"));
  }

  @Test
  void givenValidObjectWithInvalidDates_whenValidated_shouldFailValidation() {
    var request =
        SubscriptionRequest.builder()
            .amount(10)
            .endDate(LocalDate.now())
            .startDate(LocalDate.now().plusMonths(6))
            .subscriptionType(SubscriptionType.DAILY)
            .build();

    var results = validator.validate(request);
    assertNotNull(results);
    assertEquals(1, results.size());
    assertTrue(getErrorMessages(results).contains("{startDate.invalid}"));
  }

  @Test
  void givenInvalidMonthWithRequest_whenValidated_shouldFailValidation() {
    var request =
        SubscriptionRequest.builder()
            .amount(10)
            .month((short) 29)
            .startDate(LocalDate.now())
            .endDate(LocalDate.now().plusMonths(3))
            .subscriptionType(SubscriptionType.MONTHLY)
            .build();

    var results = validator.validate(request);
    assertNotNull(results);
    assertEquals(1, results.size());
    assertTrue(getErrorMessages(results).contains("{month.range.invalid}"));
  }

  @Test
  void givenInvalidSubscriptionForMonthWithRequest_whenValidated_shouldFailValidation() {
    var request =
            SubscriptionRequest.builder()
                    .amount(10)
                    .month((short) 20)
                    .startDate(LocalDate.now())
                    .endDate(LocalDate.now().plusMonths(3))
                    .subscriptionType(SubscriptionType.WEEKLY)
                    .build();

    var results = validator.validate(request);
    assertNotNull(results);
    assertEquals(1, results.size());
    assertTrue(getErrorMessages(results).contains("{subscription.type.month.invalid}"));
  }

  private List<String> getErrorMessages(
      Set<ConstraintViolation<SubscriptionRequest>> constraintViolations) {
    return constraintViolations.stream()
        .map(ConstraintViolation::getMessage)
        .collect(Collectors.toList());
  }
}
