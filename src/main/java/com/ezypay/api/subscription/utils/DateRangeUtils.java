package com.ezypay.api.subscription.utils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public final class DateRangeUtils {
  private DateRangeUtils() {}

  private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

  public static List<String> getDatesForWeeklySubscription(
      DayOfWeek weekDay, LocalDate start, LocalDate end) {

    return start
        .datesUntil(end)
        .filter(localDate -> localDate.getDayOfWeek().equals(weekDay))
        .map(date -> date.format(FORMATTER))
        .collect(Collectors.toList());
  }

  public static List<String> getDatesForMonthlySubscription(
      short month, LocalDate start, LocalDate end) {
    return start
        .datesUntil(end)
        .filter(localDate -> month == localDate.getDayOfMonth())
        .map(date -> date.format(FORMATTER))
        .collect(Collectors.toList());
  }

  public static List<String> getDatesForDailySubscription(LocalDate start, LocalDate end) {
    return start.datesUntil(end).map(date -> date.format(FORMATTER)).collect(Collectors.toList());
  }
}
