package com.ezypay.api.subscription.annotation;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(
        validatedBy = {SubscriptionValidator.class}
)
@Documented
public @interface ValidateSubscriptionRequest {
    String message() default "{request.invalid}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
