package com.ezypay.api.subscription.annotation;

import com.ezypay.api.subscription.model.SubscriptionRequest;
import com.ezypay.api.subscription.model.SubscriptionType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NotNull;
import java.time.Period;

public class SubscriptionValidator
    implements ConstraintValidator<ValidateSubscriptionRequest, SubscriptionRequest> {

  private ConstraintValidatorContext context;

  @Override
  public boolean isValid(SubscriptionRequest request, ConstraintValidatorContext context) {
    if (null != request) {
      this.context = context;
      boolean isValid = true;
      var startDate = request.getStartDate();
      var endDate = request.getEndDate();
      if (null != startDate && null != endDate) {
        if (startDate.isAfter(endDate)) {
          isValid = false;
          this.addErrorMsgId("{startDate.invalid}");
        }
        if (Period.between(startDate, endDate).getMonths() > 3) {
          isValid = false;
          this.addErrorMsgId("{date.range.invalid}");
        }
      }

      if (request.getMonth() > 0
          && !SubscriptionType.MONTHLY.equals(request.getSubscriptionType())) {
        isValid = false;
        this.addErrorMsgId("{subscription.type.month.invalid}");
      }

      if (SubscriptionType.MONTHLY == request.getSubscriptionType() && request.getMonth() < 1) {
        isValid = false;
        this.addErrorMsgId("{month.required}");
      }
      this.context = context;

      return isValid;
    }
    return false;
  }

  private void addErrorMsgId(@NotNull String errorMsgId) {
    this.context.disableDefaultConstraintViolation();
    this.context.buildConstraintViolationWithTemplate(errorMsgId).addConstraintViolation();
  }
}
