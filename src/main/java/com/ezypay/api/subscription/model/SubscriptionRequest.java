package com.ezypay.api.subscription.model;

import com.ezypay.api.subscription.annotation.ValidateSubscriptionRequest;
import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.DayOfWeek;
import java.time.LocalDate;

@Data
@Builder
@ValidateSubscriptionRequest
public class SubscriptionRequest {

  @NotNull(message = "{amount.required}")
  @Digits(integer = 10, fraction = 2, message = "{amount.invalid.format}")
  @Min(value = 1, message = "{amount.invalid}")
  private float amount;

  @NotNull(message = "{subscriptionType.required}")
  @Valid
  private SubscriptionType subscriptionType;

  private DayOfWeek dayOfWeek;

  @Max(value = 28, message = "{month.range.invalid}")
  private short month;

  @FutureOrPresent(message = "{startDate.required}")
  private LocalDate startDate;

  @FutureOrPresent(message = "{endDate.required}")
  private LocalDate endDate;
}
