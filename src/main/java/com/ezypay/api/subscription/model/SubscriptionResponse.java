package com.ezypay.api.subscription.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SubscriptionResponse {

    private float amount;

    private SubscriptionType subscriptionType;

    private List<String> invoiceDates;
}
