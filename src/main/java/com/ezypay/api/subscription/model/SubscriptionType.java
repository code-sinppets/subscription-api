package com.ezypay.api.subscription.model;

public enum SubscriptionType {
    DAILY,
    WEEKLY,
    MONTHLY;

}
