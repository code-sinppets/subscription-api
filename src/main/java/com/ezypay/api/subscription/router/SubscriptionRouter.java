package com.ezypay.api.subscription.router;

import com.ezypay.api.subscription.handler.SubscriptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class SubscriptionRouter {

  static final String BASE_URL = "/api/v1/subscriptions";

  /**
   * @param subscriptionHandler instance of {@link SubscriptionHandler}
   * @return exposes list of endpoints to access the API
   */
  @Bean
  public RouterFunction<ServerResponse> workspaceRoutes(SubscriptionHandler subscriptionHandler) {
    return nest(path(BASE_URL), route(POST(""), subscriptionHandler::createSubscription));
  }
}
