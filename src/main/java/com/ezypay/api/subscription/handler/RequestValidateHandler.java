package com.ezypay.api.subscription.handler;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.util.annotation.NonNull;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.springframework.web.reactive.function.server.ServerResponse.unprocessableEntity;

/** Java bean validator wrapper class. */
@Component
@AllArgsConstructor
public class RequestValidateHandler {

  private final Validator validator;

  /**
   * Convert the request with reference class and validate with validator service. Return converted
   * body as response if validation is passed; else return error as response
   *
   * @param onSuccess execute the function or method post successfully validation
   * @param request {@link ServerRequest}
   * @param bodyClass reference class to convert the request
   * @param <B> Reference class return type
   * @return {@link ServerResponse}
   */
  @NonNull
  public <B> Mono<ServerResponse> requireValidBody(
      Function<Mono<B>, Mono<ServerResponse>> onSuccess,
      ServerRequest request,
      Class<B> bodyClass) {
    return request
        .bodyToMono(bodyClass)
        .flatMap(
            body -> {
              var violations = this.validator.validate(body);
              if (violations.isEmpty()) {
                return onSuccess.apply(Mono.just(body));
              }
              return unprocessableEntity()
                  .bodyValue(
                      violations.stream()
                          .map(ConstraintViolation::getMessage)
                          .collect(Collectors.toUnmodifiableList()));
            });
  }
}
