package com.ezypay.api.subscription.handler;

import com.ezypay.api.subscription.model.SubscriptionRequest;
import com.ezypay.api.subscription.model.SubscriptionResponse;
import com.ezypay.api.subscription.utils.DateRangeUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.util.annotation.NonNull;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SubscriptionHandler {

  private final RequestValidateHandler requestValidateHandler;

  @NonNull
  public Mono<ServerResponse> createSubscription(ServerRequest request) {
    return this.requestValidateHandler.requireValidBody(
        body ->
            body.flatMap(this::createResponse)
                .onErrorResume(ex -> ServerResponse.status(500).bodyValue(ex.getMessage())),
        request,
        SubscriptionRequest.class);
  }

  private Mono<ServerResponse> createResponse(SubscriptionRequest request) {
    return ServerResponse.status(201)
        .bodyValue(
            SubscriptionResponse.builder()
                .amount(request.getAmount())
                .invoiceDates(getDates(request))
                .subscriptionType(request.getSubscriptionType())
                .build());
  }

  private List<String> getDates(SubscriptionRequest request) {
    if (null == request.getEndDate()) {
      request.setEndDate(request.getStartDate().plusMonths(3));
    }
    return switch (request.getSubscriptionType()) {
      case MONTHLY -> DateRangeUtils.getDatesForMonthlySubscription(request.getMonth(), request.getStartDate(), request.getEndDate());
      case WEEKLY -> DateRangeUtils.getDatesForWeeklySubscription(request.getDayOfWeek(), request.getStartDate(), request.getEndDate());
      case DAILY ->  DateRangeUtils.getDatesForDailySubscription(request.getStartDate(), request.getEndDate());
    };
  }
}
